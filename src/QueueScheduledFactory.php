<?php

namespace Drupal\scheduled_queue;

use Drupal\Core\Queue\QueueDatabaseFactory;

/**
 * Defines the key/value store factory for the database backend.
 */
class QueueScheduledFactory extends QueueDatabaseFactory {

  /**
   * {@inheritdoc}
   */
  public function get($name) {
    return new ScheduledQueue($name, $this->connection);
  }

}
